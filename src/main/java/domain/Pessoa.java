package domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "tab_pessoa")
public class Pessoa {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cpf;
	
	@Column(length = 40, nullable = false)
	private String nome;
	
	@Column(length = 250, nullable = true)
	private String email;
	
	@OneToMany(mappedBy = "comprador")
	private List<Veiculo> veiculos;


	public Pessoa(){
		
	}
	
	public Pessoa(String cpf, String nome, String email) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.email = email;
	}



	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public List<Veiculo> getVeiculos() {
		return veiculos;
	}


	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}
	
	
	
}
