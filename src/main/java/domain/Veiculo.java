package domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import enums.TipoVeiculoEnum;

@Entity(name = "tab_veiculo")
@Table(uniqueConstraints = {
@UniqueConstraint(name = "un_veiculo", columnNames = { "chassis" }) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Veiculo {
	
		
	@Id
	@Column(columnDefinition = "char(7)", nullable = false)
	private String placa;
	
	@Column(columnDefinition = "char(17)", nullable = false)
	private String chassis;
	
	@OneToOne
	private Modelo modelo;
	
	@Column(columnDefinition = "char(2)", nullable = false)
	private TipoVeiculoEnum tipo;
	
	@Column(nullable = false)
	private Date dataCompra;
	
	@OneToOne
	private Pessoa comprador;
	
	public Veiculo(){
		
	}

	public Veiculo(String placa, String chassis, Modelo modelo, TipoVeiculoEnum tipo, Date dataCompra,
			Pessoa comprador) {
		super();
		this.placa = placa;
		this.chassis = chassis;
		this.modelo = modelo;
		this.tipo = tipo;
		this.dataCompra = dataCompra;
		this.comprador = comprador;
	}
	

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}
}
